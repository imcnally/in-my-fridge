/*
 * Default Gruntfile for AppGyver Steroids
 * http://www.appgyver.com
 *
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  grunt.initConfig({
    coffee : {
      compile : {
        files : {
          'dist/modules/modules.js' : ['app/directives/**/*.coffee', 'app/factories/**/*.coffee', 'app/filters/**/*.coffee', 'app/services/**/*.coffee']
        }
      },
      options : {
        join : false
      }
    }
  });

  grunt.loadNpmTasks("grunt-steroids");
  grunt.loadNpmTasks("grunt-config-coffee")

  grunt.registerTask("default", ["steroids-make", "steroids-compile-sass", "coffee"]);

};
