In My Fridge
============

A planner for your fridge, so nothing goes to waste.

In the [iOS App Store](https://itunes.apple.com/us/app/in-my-fridge/id735487498)

Built using AppGyver's [Steroids](http://developers.appgyver.com)
