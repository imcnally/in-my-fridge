app.factory 'ListItem', ->
  (name, quantity, date) ->
    name : name or 'New Item'
    quantity : quantity or 1
    purchase_date : (new Date).toString()