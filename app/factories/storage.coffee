app.factory 'Storage', ->
  storage = window.localStorage

  set : (key, value) ->
    storage.setItem key, JSON.stringify(value)

  get : (key, defaultValue) ->
    value = storage.getItem(key)
    if value then JSON.parse(value) else defaultValue