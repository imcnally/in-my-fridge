app.factory 'NavButton', ->
  (title, tapCallback, imagePath) ->
    button = new steroids.buttons.NavigationBarButton()
    button.title = title if title
    button.imagePath = imagePath if imagePath
    button.onTap = tapCallback or ->
    button