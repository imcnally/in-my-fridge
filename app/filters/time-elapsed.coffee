app.filter 'timeElapsed', ->
  (date) ->
    days = moment(date).diff(moment(), 'days')
    days = 0 if days > 0
    "#{Math.abs days} d"