steroids.view.navigationBar.show 'In My Fridge'

drawer = new steroids.views.WebView 'views/drawer/index.html'
drawer.preload()

# TODO: migrate to multi-app requires storage key changes.
# remove when safe
if localStorage.fridgeItems
  localStorage.setItem 'fridge', localStorage.fridgeItems
  localStorage.removeItem 'fridgeItems'

capitalize = (str) -> str[0].toUpperCase() + str[1..-1]

app.controller 'Items', ($scope, ListItem, NavButton, Storage, sortService) ->

  $scope.sortOption = sortService.getSortPreference()

  $scope.collections =
    fridge : 'fridge'
    pantry : 'pantry'
    freezer : 'freezer'

  $scope.collectionTitle = $scope.collections.fridge

  $scope.setListTitle = (title) ->
    title = capitalize title
    steroids.view.navigationBar.show "In My #{title}"

  window.addEventListener 'message', (message={}) ->
    collection = message.data?.collection
    sortOption = message.data?.sortOption
    $scope.$broadcast('collectionChange', collection) if collection
    $scope.$broadcast('sortChange', sortOption) if sortOption

  $scope.loadItems = ->
    $scope.items = Storage.get($scope.collectionTitle, [])
    $scope.$digest() unless $scope.$$phase

  $scope.loadItems()

  $scope.selectNewItem = (newItem) ->
    document.querySelector("""[data-sort="#{newItem.purchase_date}"]""").click()

  $scope.addItem = ->
    newItem = ListItem()
    $scope.items = [newItem].concat($scope.items)
    $scope.$digest() unless $scope.$$phase
    $scope.selectNewItem(newItem)

  $scope.onMenuButtonClick = ->
    action = if $scope.isDrawerOpen then 'hide' else 'show'
    steroids.drawers[action](drawer)
    $scope.isDrawerOpen = !$scope.isDrawerOpen

  $scope.buttons =
    add : NavButton null, $scope.addItem, '/icons/add@2x.png'
    openDrawer : NavButton null, $scope.onMenuButtonClick, '/icons/menu@2x.png'

  steroids.on 'ready', ->
    steroids.view.navigationBar.setButtons
      left : [$scope.buttons.openDrawer]
      right : [$scope.buttons.add]

  $scope.removeItem = (item) ->
    $scope.items = _.without $scope.items, item
    $scope.$digest() unless $scope.$$phase

  $scope.syncItems = ->
    Storage.set $scope.collectionTitle, $scope.items

  $scope.$watch 'items', _.throttle($scope.syncItems, 300), true

  $scope.$watch 'sortOption', sortService.save

  $scope.editDate = (item) ->
    window.datePicker.show
      date : new Date(item.purchase_date)
      mode : 'date'
      allowFutureDates : false
    , (date) ->
      item.purchase_date = date
      $scope.$digest() unless $scope.$$phase

  $scope.$on 'collectionChange', (_event, collectionTitle) ->
    steroids.drawers.hide drawer
    $scope.isDrawerOpen = !$scope.isDrawerOpen
    return if collectionTitle is $scope.collectionTitle
    $scope.setListTitle(collectionTitle)
    $scope.collectionTitle = collectionTitle
    $scope.loadItems()

  $scope.$on 'sortChange', (_event, sortOption) ->
    $scope.sortOption = sortOption
    $scope.$digest() unless $scope.$$phase