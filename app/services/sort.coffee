app.service 'sortService', (Storage) ->

  DEFAULT_SORT = '-purchase_date'

  STORAGE_KEY = 'sortOption'

  sortService =

    getSortPreference : ->
      Storage.get STORAGE_KEY, DEFAULT_SORT

    save : (preference) ->
      Storage.set STORAGE_KEY, preference