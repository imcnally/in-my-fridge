app.service 'messageService', ->
  messageService =
    post : (message = {}) ->
      window.postMessage message, '*'
