app.directive 'collectionSwitch', (messageService) ->
  (scope, element, attributes) ->
    message =
      collection : attributes.collectionSwitch
    Hammer(element[0]).on 'tap', ->
      messageService.post message