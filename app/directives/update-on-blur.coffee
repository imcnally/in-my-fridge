app.directive 'updateOnBlur', ->
  require : 'ngModel'
  link : (scope, element, attributes, ngModelController) ->
    element.unbind 'input'
    element.bind 'blur', ->
      scope.$apply ->
        ngModelController.$setViewValue element.val()