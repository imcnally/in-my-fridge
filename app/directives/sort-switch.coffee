app.directive 'sortSwitch', (messageService, sortService) ->
  (scope, element, attributes) ->
    element.bind 'change', (event) =>
      sortOption = event.currentTarget.value
      message = {sortOption}
      messageService.post message

    if attributes.setSortPreference?
      # set stored preference on directive load
      preference = sortService.getSortPreference()
      document.querySelector("""[value="#{preference}"]""")?.selected = true