app.directive 'swipeToDelete', ->
  (scope, element, attributes) ->
    Hammer(element[0]).on 'swipeleft', ->
      element.addClass 'deleting'

    Hammer(element[0]).on 'swiperight', ->
      element.removeClass 'deleting'